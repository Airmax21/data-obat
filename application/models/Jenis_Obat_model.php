<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jenis_Obat_model extends CI_Model
{
    private $_table = "tb_jenis_obat";

    public $nama_jenis_obat;

    public function read()
    {
        return $this->db->get($this->_table)->result();
    }

    public function readById($id_jenis_obat)
    {
        return $this->db->get_where($this->_table, ['id_jenis_obat' => $id_jenis_obat])->result();
    }
    public function readByName($nama_jenis_obat)
    {
        return $this->db->get_where($this->_table, ['nama_jenis_obat' => $nama_jenis_obat])->result();
    }

    public function create()
    {
        $post = $this->input->post();
        $this->nama_jenis_obat = $post['nama'];
        $this->db->insert($this->_table, $this);
    }

    public function update($id_jenis_obat)
    {
        $post = $this->input->post();
        $this->nama_jenis_obat = $post['nama'];
        $this->db->update($this->_table, $this, array('id_jenis_obat' => $id_jenis_obat));
    }

    public function delete($id_jenis_obat)
    {
        $this->db->delete($this->_table, ['id_jenis_obat'=> $id_jenis_obat]);
    }

    public function count()
    {
        $data = [
            'jumlah' => $this->db->query('SELECT COUNT(*) as jumlah FROM tb_jenis_obat')->result()
        ];
        return $data;
    }
}