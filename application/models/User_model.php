<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_model extends CI_Model
{
    private $_table = "tb_user";

    public $username;
    public $password;
    public $fullname;
    public $is_active = '0';

    public function read()
    {
        return $this->db->get($this->_table)->result();
    }

    public function readById($id_user)
    {
        return $this->db->get_where($this->_table, ['id_user' => $id_user])->result();
    }
    public function readByUsername($username)
    {
        return $this->db->get_where($this->_table, ['username' => $username])->result();
    }

    public function create()
    {
        $post = $this->input->post();
        $this->username = $post['username'];
        $this->password = md5($post['password']);
        $this->fullname = $post['fullname'];
        $this->db->insert($this->_table, $this);
    }

    public function update($id_user)
    {
        $data = $this->readById($id_user);
        $post = $this->input->post();
        $this->username = $post['username'];
        $this->fullname = $post['fullname'];
        if($post['password']){
            $this->password = md5($post['password']);
        }
        else {
            $this->password = $data[0]->password;
        }
        $this->db->update($this->_table, $this, array('id_user' => $id_user));
    }

    public function aktivasi($id_user)
    {
        $this->db->update($this->_table,['is_active'=>'1'], ['id_user'=> $id_user]);
    }

    public function delete($id_user)
    {
        $this->db->delete($this->_table, ['id_user'=> $id_user]);
    }

    public function login()
    {
        $post = $this->input->post();
        $this->db->where([
            'username' => $post['username'],
            'password' => md5($post['password']),
            'is_active' => '1'
        ]);
        return $this->db->get($this->_table)->row();
    }

    public function count()
    {
        $data = [
            'jumlah' => $this->db->query('SELECT COUNT(*) as jumlah FROM tb_user')->result(),
            'yang_aktif' => $this->db->query("SELECT COUNT(*) as jumlah FROM tb_user WHERE is_active='1'")->result(),
            'belum_aktif' => $this->db->query("SELECT COUNT(*) as jumlah FROM tb_user WHERE is_active='0'")->result()
        ];
        return $data;
    }
}