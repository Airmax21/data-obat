<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Obat_model extends CI_Model
{
    private $_table = "tb_obat";

    public $id_jenis_obat;
    public $nama_obat;
    public $satuan;
    public $harga;
    public $stok;
    public $tanggal_expired;
    public $gambar;

    public function read()
    {
        return $this->db->query("SELECT * FROM tb_obat 
        JOIN tb_jenis_obat ON tb_obat.id_jenis_obat = tb_jenis_obat.id_jenis_obat")
            ->result();
    }

    public function readById($id_obat)
    {
        return $this->db->query("SELECT * FROM tb_obat 
        JOIN tb_jenis_obat ON tb_obat.id_jenis_obat = tb_jenis_obat.id_jenis_obat 
        WHERE id_obat = {$id_obat}")
            ->result();
    }

    public function create()
    {
        $post = $this->input->post();
        $this->id_jenis_obat = $post['jenisobat'];
        $this->nama_obat = $post['nama'];
        $this->satuan = $post['satuan'];
        $this->harga = $post['harga'];
        $this->stok = $post['stok'];
        $this->tanggal_expired = $post['expired'];
        if (!empty($_FILES['gambar']['name'])) {
            $config['upload_path']          = 'files/upload/';
            $config['allowed_types']        = 'gif|jpg|jpeg|png';
            $config['file_name']            = $_FILES['gambar']['name'];
            $config['overwrite']            = true;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if ($this->upload->do_upload('gambar')) {
                $this->gambar =  $this->upload->data('file_name');
            }
        }
        $this->db->insert($this->_table, $this);
    }

    public function update($id_obat)
    {
        $data = $this->readById($id_obat);
        $post = $this->input->post();
        $this->id_jenis_obat = $post['jenisobat'];
        $this->nama_obat = $post['nama'];
        $this->satuan = $post['satuan'];
        $this->harga = $post['harga'];
        $this->stok = $post['stok'];
        $this->tanggal_expired = $post['expired'];
        $this->gambar = $post['gambar'];
        if (!empty($_FILES['gambar']['name'])) {
            $config['upload_path']          = 'files/upload/';
            $config['allowed_types']        = 'gif|jpg|jpeg|png';
            $config['file_name']            = $_FILES['gambar']['name'];
            $config['overwrite']            = true;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if ($this->upload->do_upload('gambar')) {
                $this->gambar =  $this->upload->data('file_name');
            } else {
                $this->gambar = $data[0]->gambar;
            }
        } else {
            $this->gambar = $data[0]->gambar;
        }
        $this->db->update($this->_table, $this, array('id_obat' => $id_obat));
    }

    public function delete($id_obat)
    {
        $data = $this->readById($id_obat);
        $this->db->delete($this->_table, ['id_obat' => $id_obat]);
        $this->load->helper("file");
        delete_files('files/upload' . $data[0]->gambar);
    }

    public function count()
    {
        $data = [
            'jumlah' => $this->db->query('SELECT COUNT(*) as jumlah FROM tb_obat')->result(),
            'yang_expired' => $this->db->query("SELECT COUNT(*) as jumlah FROM tb_obat WHERE CURDATE() < tanggal_expired")->result(),
            'belum_expired' => $this->db->query("SELECT COUNT(*) as jumlah FROM tb_obat WHERE CURDATE() >= tanggal_expired")->result()
        ];
        return $data;
    }

    public function search()
    {
        $post = $this->input->post();
        $where = '1=1 ';
        if ($post['jenisobat'] != '-') {
            $where .= "AND o.id_jenis_obat =".$post['jenisobat'];   
        }
        if($post['nama']){
            $where .= "AND o.nama_obat = '{$post['nama']}'";
        }
        if($post['dari'] && $post['sampai']){
            $where .= "AND tanggal_expired BETWEEN '{$post['dari']}' AND '{$post['sampai']}'";
        }
        return $this->db->query("SELECT * FROM tb_obat o 
        JOIN tb_jenis_obat j ON o.id_jenis_obat = j.id_jenis_obat 
        WHERE ".$where)->result(); 
        // AND nama_obat LIKE '%{$this->nama_obat}%' 
        // AND tanggal_expired BETWEEN '{$dari}' AND '{$sampai}'"
    }
}
