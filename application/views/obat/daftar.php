<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Manajemen Obat</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url() ?>plugins/fontawesome-free/css/all.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url() ?>plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url() ?>plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url() ?>dist/css/adminlte.min.css">
</head>

<body class="hold-transition sidebar-mini">
  <div class="wrapper">
    <?php $this->load->view('layout/navbar'); ?>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>DataTables</h1>
            </div>
            <div class="col-sm-6">

            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h3 class="card-title">Table Obat</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalTambah">
                    Tambah Obat
                  </button>
                  <a href=<?php echo site_url('obat/cetakpdf'); ?>>
                    <button type="button" class="btn btn-primary">
                      Export PDF
                    </button>
                  </a>
                  <a href=<?php echo site_url('obat/cetakxls'); ?>>
                    <button type="button" class="btn btn-primary">
                      Export XLS
                    </button>
                  </a>
                  <!-- Modal -->
                  <div class="modal fade" id="modalTambah" tabindex="-1" role="dialog" aria-labelledby="modalUpdateTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="modalUpdateTitle">Tambah Obat</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <form action=<?php echo base_url('obat/tambah') ?> method="post" enctype="multipart/form-data">
                          <div class="modal-body">
                            <div class="form-group">
                              <label>Jenis Obat</label>
                              <select name="jenisobat" class="form-control">
                                <?php foreach ($jenisobat as $jenis) : ?>
                                  <option value=<?php echo $jenis->id_jenis_obat ?>><?php echo $jenis->nama_jenis_obat ?></option>
                                <?php endforeach; ?>
                              </select>
                            </div>
                            <div class="form-group">
                              <label>Nama Obat</label>
                              <input type="text" name="nama" class="form-control" placeholder="Masukkan Nama Obat">
                            </div>
                            <div class="form-group">
                              <label>Satuan</label>
                              <input type="text" name="satuan" class="form-control" placeholder="Masukkan Satuan">
                            </div>
                            <div class="form-group">
                              <label>Harga</label>
                              <input type="number" name="harga" class="form-control" placeholder="Masukkan Harga Obat">
                            </div>
                            <div class="form-group">
                              <label>Stok</label>
                              <input type="number" name="stok" class="form-control" placeholder="Masukkan Stok">
                            </div>
                            <div class="form-group">
                              <label>Tanggal Expired</label>
                              <input type="date" name="expired" class="form-control" placeholder="Masukkan Tanggal">
                            </div>
                            <div class="form-group">
                              <label>Gambar</label>
                              <input type="file" name="gambar" class="form-control" placeholder="Masukkan Obat">
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Tambah</button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>Id Obat</th>
                        <th>Jenis Obat</th>
                        <th>Nama Obat</th>
                        <th>Satuan</th>
                        <th>Harga</th>
                        <th>Stok</th>
                        <th>Jumlah Harga</th>
                        <th>Tanggal Expired</th>
                        <th>Gambar</th>
                        <th>Modif</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($obats as $obat) : ?>
                        <tr>
                          <td><?php echo $obat->id_obat ?></td>
                          <td><?php echo $obat->nama_jenis_obat ?></td>
                          <td><?php echo $obat->nama_obat ?></td>
                          <td><?php echo $obat->satuan ?></td>
                          <td><?php echo $obat->harga ?></td>
                          <td><?php echo $obat->stok ?></td>
                          <td><?php echo $obat->stok * $obat->harga ?></td>
                          <td><?php echo $obat->tanggal_expired ?></td>
                          <td><img width="150px" src="<?php echo site_url() ?>files/upload/<?php echo $obat->gambar ?>" alt="<?php echo $obat->gambar ?>" ></td>
                          <td>
                            <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modalUpdate<?php echo $obat->id_obat ?>">
                              Update
                            </button>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modalDelete<?php echo $obat->id_obat ?>">
                              Delete
                            </button>
                          </td>
                        </tr>
                        <div class="modal fade" id="modalUpdate<?php echo $obat->id_obat ?>" tabindex="-1" role="dialog" aria-labelledby="modalUpdateTitle" aria-hidden="true">
                          <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="modalUpdateTitle">Update User</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <form action=<?php echo base_url('obat/update') . '/' . $obat->id_obat; ?> method="post" enctype="multipart/form-data">
                                <div class="modal-body">
                                  <div class="form-group">
                                    <label>ID Obat</label>
                                    <input type="text" disabled class="form-control" value=<?php echo $obat->id_obat ?>>
                                  </div>
                                  <div class="form-group">
                                    <label>Jenis Obat</label>
                                    <select name="jenisobat" class="form-control">
                                      <?php foreach ($jenisobat as $jenis) : ?>
                                        <option value=<?php echo $jenis->id_jenis_obat ?>><?php echo $jenis->nama_jenis_obat ?></option>
                                      <?php endforeach; ?>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label>Nama Obat</label>
                                    <input type="text" name="nama" class="form-control" value=<?php echo $obat->nama_obat ?> placeholder="Masukkan Nama Obat">
                                  </div>
                                  <div class="form-group">
                                    <label>Satuan</label>
                                    <input type="text" name="satuan" class="form-control" value=<?php echo $obat->satuan ?> placeholder="Masukkan Satuan">
                                  </div>
                                  <div class="form-group">
                                    <label>Harga</label>
                                    <input type="number" name="harga" class="form-control" value=<?php echo $obat->harga ?> placeholder="Masukkan Harga Obat">
                                  </div>
                                  <div class="form-group">
                                    <label>Stok</label>
                                    <input type="number" name="stok" class="form-control" value=<?php echo $obat->stok ?> placeholder="Masukkan Stok">
                                  </div>
                                  <div class="form-group">
                                    <label>Tanggal Expired</label>
                                    <input type="date" name="expired" class="form-control" value=<?php echo $obat->tanggal_expired ?> placeholder="Masukkan Obat">
                                  </div>
                                  <div class="form-group">
                                    <label>Gambar</label>
                                    <input type="file" name="gambar" class="form-control" placeholder="Masukkan Obat">
                                  </div>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                  <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                              </form>
                            </div>
                          </div>
                        </div>
                        <div class="modal fade" id="modalDelete<?php echo $obat->id_obat ?>" tabindex="-1" role="dialog" aria-labelledby="modalUpdateTitle" aria-hidden="true">
                          <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="modalUpdateTitle">Delete Obat</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                              <div class="modal-body">
                                <div class="form-group">
                                  <label>ID Obat</label>
                                  <input type="text" disabled class="form-control" value=<?php echo $obat->id_obat ?>>
                                </div>
                                <div class="form-group">
                                  <label>Jenis Obat</label>
                                  <?php foreach ($jenisobat as $jenis) :
                                    if ($jenis->id_jenis_obat === $obat->id_jenis_obat) : ?>
                                      <input type="text" name="nama" class="form-control" disabled value=<?php echo $jenis->nama_jenis_obat ?>>
                                    <?php endif; ?>
                                  <?php endforeach; ?>
                                </div>
                                <div class="form-group">
                                  <label>Nama Obat</label>
                                  <input type="text" name="nama" class="form-control" disabled value=<?php echo $obat->nama_obat ?>>
                                </div>
                                <div class="form-group">
                                  <label>Satuan</label>
                                  <input type="text" name="satuan" class="form-control" disabled value=<?php echo $obat->satuan ?>>
                                </div>
                                <div class="form-group">
                                  <label>Harga</label>
                                  <input type="number" name="harga" class="form-control" disabled value=<?php echo $obat->harga ?>>
                                </div>
                                <div class="form-group">
                                  <label>Stok</label>
                                  <input type="number" name="stok" class="form-control" disabled value=<?php echo $obat->stok ?>>
                                </div>
                                <div class="form-group">
                                  <label>Tanggal Expired</label>
                                  <input type="date" name="expired" class="form-control" disabled value=<?php echo $obat->tanggal_expired ?>>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                  <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modalKonfirmasiDelete<?php echo $obat->id_obat ?>">
                                    Delete
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal fade" id="modalKonfirmasiDelete<?php echo $obat->id_obat ?>" tabindex="-1" role="dialog" aria-labelledby="modalUpdateTitle" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <h5 class="modal-title" id="modalUpdateTitle">Delete Obat</h5>
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <form action=<?php echo base_url('obat/delete') . '/' . $obat->id_obat; ?> method="post" enctype="multipart/form-data">
                                  <div class="modal-body">
                                    <p>Apakah anda yakin ingin menghapus obat <?php echo $obat->nama_obat ?>?</p>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>
                        <?php endforeach; ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>Id Obat</th>
                        <th>Jenis Obat</th>
                        <th>Nama Obat</th>
                        <th>Satuan</th>
                        <th>Harga</th>
                        <th>Stok</th>
                        <th>Jumlah Harga</th>
                        <th>Tanggal Expired</th>
                        <th>Gambar</th>
                        <th>Modif</th>
                      </tr>
                    </tfoot>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->

              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->

  <!-- jQuery -->
  <script src="<?php echo base_url() ?>plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="<?php echo base_url() ?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- DataTables  & Plugins -->
  <script src="<?php echo base_url() ?>plugins/datatables/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url() ?>plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?php echo base_url() ?>plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
  <script src="<?php echo base_url() ?>plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
  <script src="<?php echo base_url() ?>plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
  <script src="<?php echo base_url() ?>plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
  <script src="<?php echo base_url() ?>plugins/jszip/jszip.min.js"></script>
  <script src="<?php echo base_url() ?>plugins/pdfmake/pdfmake.min.js"></script>
  <script src="<?php echo base_url() ?>plugins/pdfmake/vfs_fonts.js"></script>
  <script src="<?php echo base_url() ?>plugins/datatables-buttons/js/buttons.html5.min.js"></script>
  <script src="<?php echo base_url() ?>plugins/datatables-buttons/js/buttons.print.min.js"></script>
  <script src="<?php echo base_url() ?>plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
  <!-- AdminLTE App -->
  <script src="<?php echo base_url() ?>dist/js/adminlte.min.js"></script>
  <script>
    $(function() {
      $("#example1").DataTable({
        "responsive": true,
        "lengthChange": false,
        "autoWidth": false,
        "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
      });
    });
  </script>
</body>

</html>