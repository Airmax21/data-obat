<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Dashboard</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url() ?>plugins/fontawesome-free/css/all.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo base_url() ?>plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url() ?>dist/css/adminlte.min.css">
</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        <?php $this->load->view('layout/navbar'); ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Selamat Datang, <?php echo $user ?></h1>
                        </div>
                        <div class="col-sm-6">

                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-12">
                            <a href=<?php echo base_url('user'); ?>>
                                <div class="info-box bg-info">

                                    <span class="info-box-icon"><img src="<?php echo base_url() ?>dist/img/akun.png" alt="user" srcset=""></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Users</span>
                                        <span class="info-box-number"><?php echo $jumlahjenis['jumlah'][0]->jumlah ?></span>
                                        <span class="progress-description">
                                            User aktif &emsp; <?php echo $jumlahuser['yang_aktif'][0]->jumlah ?>
                                        </span>
                                        <span class="progress-description">
                                            User belum aktif &emsp; <?php echo $jumlahuser['belum_aktif'][0]->jumlah ?>
                                        </span>
                                    </div>

                                    <!-- /.info-box-content -->
                                </div>
                            </a>
                            <!-- /.info-box -->
                        </div>
                        <div class="col-md-4 col-sm-6 col-12">
                            <a href=<?php echo base_url('jenisobat'); ?>>
                                <div class="info-box bg-success">

                                    <span class="info-box-icon"><img src="<?php echo base_url() ?>dist/img/jenis.png" alt="jenis obat" srcset=""></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Jenis Obat</span>
                                        <span class="info-box-number"><?php echo $jumlahjenis['jumlah'][0]->jumlah ?></span>
                                        <span class="progress-description">
                                            <br>
                                        </span>
                                        <span class="progress-description">
                                            <br>
                                        </span>
                                    </div>

                                    <!-- /.info-box-content -->
                                </div>
                            </a>
                            <!-- /.info-box -->
                        </div>
                        <div class="col-md-4 col-sm-6 col-12">
                            <a href=<?php echo base_url('obat'); ?>>
                                <div class="info-box bg-danger">

                                    <span class="info-box-icon"><img src="<?php echo base_url() ?>dist/img/obat.png" alt="obat" srcset=""></i></span>

                                    <div class="info-box-content">
                                        <span class="info-box-text">Obat</span>
                                        <span class="info-box-number"><?php echo $jumlahobat['jumlah'][0]->jumlah ?></span>
                                        <span class="progress-description">
                                            Obat Expired &emsp; <?php echo $jumlahobat['yang_expired'][0]->jumlah ?>
                                        </span>
                                        <span class="progress-description">
                                            Obat Belum Expired &emsp; <?php echo $jumlahobat['belum_expired'][0]->jumlah ?>
                                        </span>
                                    </div>

                                    <!-- /.info-box-content -->
                                </div>
                            </a>
                            <!-- /.info-box -->
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">Table Obat</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <form action=<?php echo base_url() ?> method="post" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label>Jenis Obat</label>
                                            <select name="jenisobat" class="form-control">
                                                <option value='-'> - </option>
                                                <?php foreach ($jenisobat as $jenis) : ?>
                                                    <option value=<?php echo $jenis->id_jenis_obat ?>><?php echo $jenis->nama_jenis_obat ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Nama Obat</label>
                                            <input type="text" name="nama" class="form-control" placeholder="Masukkan Nama Obat">
                                        </div>
                                        <div class="form-group">
                                            <label>Tanggal Expired</label>
                                            <input type="date" name="dari" class="form-control" width="200" placeholder="Masukkan Tanggal">
                                            <label>&emsp;S/D</label>
                                            <input type="date" name="sampai" class="form-control" width="200" placeholder="Masukkan Tanggal">
                                        </div>
                                        <button type="submit" class="btn btn-primary">Cari</button>
                                    </form>
                                    <br>
                                    <a href=<?php echo site_url('obat/cetakpdf'); ?>>
                                        <button type="button" class="btn btn-primary">
                                            Export PDF
                                        </button>
                                    </a>
                                    <a href=<?php echo site_url('obat/cetakxls'); ?>>
                                        <button type="button" class="btn btn-primary">
                                            Export XLS
                                        </button>
                                    </a>
                                    <table id="example2" class="table table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>Id Obat</th>
                                                <th>Jenis Obat</th>
                                                <th>Nama Obat</th>
                                                <th>Satuan</th>
                                                <th>Harga</th>
                                                <th>Stok</th>
                                                <th>Jumlah Harga</th>
                                                <th>Tanggal Expired</th>
                                                <th>Gambar</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($obats as $obat) : ?>
                                                <tr>
                                                    <td><?php echo $obat->id_obat ?></td>
                                                    <td><?php echo $obat->nama_jenis_obat ?></td>
                                                    <td><?php echo $obat->nama_obat ?></td>
                                                    <td><?php echo $obat->satuan ?></td>
                                                    <td><?php echo $obat->harga ?></td>
                                                    <td><?php echo $obat->stok ?></td>
                                                    <td><?php echo $obat->stok * $obat->harga ?></td>
                                                    <td><?php echo $obat->tanggal_expired ?></td>
                                                    <td><img width="150px" src="<?php echo site_url() ?>files/upload/<?php echo $obat->gambar ?>" alt="<?php echo $obat->gambar ?>"></td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Id Obat</th>
                                                <th>Jenis Obat</th>
                                                <th>Nama Obat</th>
                                                <th>Satuan</th>
                                                <th>Harga</th>
                                                <th>Stok</th>
                                                <th>Jumlah Harga</th>
                                                <th>Tanggal Expired</th>
                                                <th>Gambar</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->

                            <!-- /.card -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src="<?php echo base_url() ?>plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="<?php echo base_url() ?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- DataTables  & Plugins -->
    <script src="<?php echo base_url() ?>plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url() ?>plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url() ?>plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo base_url() ?>plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
    <script src="<?php echo base_url() ?>plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url() ?>plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
    <script src="<?php echo base_url() ?>plugins/jszip/jszip.min.js"></script>
    <script src="<?php echo base_url() ?>plugins/pdfmake/pdfmake.min.js"></script>
    <script src="<?php echo base_url() ?>plugins/pdfmake/vfs_fonts.js"></script>
    <script src="<?php echo base_url() ?>plugins/datatables-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url() ?>plugins/datatables-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url() ?>plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url() ?>dist/js/adminlte.min.js"></script>
    <script>
        $(function() {
            $("#example1").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });
    </script>
</body>

</html>