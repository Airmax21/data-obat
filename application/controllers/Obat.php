<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Obat extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Obat_model");
        $this->load->model("Jenis_Obat_model");
        $this->load->library("pdf");
        $this->load->helper(array('url', 'cookie', 'download'));
    }
    public function index()
    {
        if (get_cookie('username')) {
            $data['obats'] = $this->Obat_model->read();
            $data['jenisobat'] = $this->Jenis_Obat_model->read();
            $this->load->view('obat/daftar', $data);
        } else {
            redirect('/login');
            return;
        }
    }
    public function tambah()
    {
        $obat = $this->Obat_model;
        $jenisobat = $this->Jenis_Obat_model;
        $data['jenisobat'] = $jenisobat->read();
        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            $obat->create();
            $this->session->set_flashdata('success', 'Berhasil Disimpan');
        }
        redirect('/obat');
    }
    public function update($id_obat)
    {
        $obat = $this->Obat_model;
        $data['obat'] = $obat->readById($id_obat);
        $jenisobat = $this->Jenis_Obat_model;

        $jenisobats = $jenisobat->read();
        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            $obat->update($id_obat);
            $this->session->set_flashdata('success', 'Berhasil Diubah');
        }
        redirect('/obat');
    }
    public function delete($id_obat)
    {
        $obat = $this->Obat_model;
        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            $obat->delete($id_obat);
            $this->session->set_flashdata('success', 'Berhasil Dihapus');
        }
        redirect('/obat');
    }
    public function cetakpdf()
    {
        $obats = $this->Obat_model;
        error_reporting(0);
        $pdf = new FPDF('L', 'mm', 'Letter');
        $pdf->AddPage();
        $pdf->SetFont('Arial', 'B', 16);
        $pdf->Cell(0, 7, 'DAFTAR OBAT', 0, 1, 'C');
        $pdf->Cell(10, 7, '', 0, 1);
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(10, 6, 'ID', 1, 0, 'C');
        $pdf->Cell(60, 6, 'Jenis Obat', 1, 0, 'C');
        $pdf->Cell(60, 6, 'Nama Obat', 1, 0, 'C');
        $pdf->Cell(30, 6, 'Satuan', 1, 0, 'C');
        $pdf->Cell(30, 6, 'Harga', 1, 0, 'C');
        $pdf->Cell(30, 6, 'Stok', 1, 0, 'C');
        $pdf->Cell(40, 6, 'Tanggal Expired', 1, 1, 'C');
        $pdf->SetFont('Arial', '', 10);
        foreach ($obats->read() as $obat) {
            $pdf->Cell(10, 6, $obat->id_obat, 1, 0, 'C');
            $pdf->Cell(60, 6, $this->Jenis_Obat_model->readById($obat->id_jenis_obat)[0]->nama_jenis_obat, 1, 0, 'C');
            $pdf->Cell(60, 6, $obat->nama_obat, 1, 0, 'C');
            $pdf->Cell(30, 6, $obat->satuan, 1, 0, 'C');
            $pdf->Cell(30, 6, 'Rp.' . $obat->harga, 1, 0, 'C');
            $pdf->Cell(30, 6, $obat->stok, 1, 0, 'C');
            $pdf->Cell(40, 6, $obat->tanggal_expired, 1, 1, 'C');
        }
        $pdf->Output('I', 'Obat.pdf');
    }
    public function cetakxls()
    {
        $fileName = 'Obat.xlsx';
        $obats = $this->Obat_model->read();
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'ID');
        $sheet->setCellValue('B1', 'Jenis Obat');
        $sheet->setCellValue('C1', 'Nama Obat');
        $sheet->setCellValue('D1', 'Satuan');
        $sheet->setCellValue('E1', 'Harga');
        $sheet->setCellValue('F1', 'Stok');
        $sheet->setCellValue('G1', 'Tanggal Expired');
        $rows = 2;
        foreach ($obats as $obat) {
            $sheet->setCellValue('A' . $rows, $obat->id_obat);
            $sheet->setCellValue('B' . $rows, $this->Jenis_Obat_model->readById($obat->id_jenis_obat)[0]->nama_jenis_obat);
            $sheet->setCellValue('C' . $rows, $obat->nama_obat);
            $sheet->setCellValue('D' . $rows, $obat->satuan);
            $sheet->setCellValue('E' . $rows, $obat->harga);
            $sheet->setCellValue('F' . $rows, $obat->stok);
            $sheet->setCellValue('G' . $rows, $obat->tanggal_expired);
            $rows++;
        }
        $styleArray = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
        ];
        $rows = $rows - 1;
        $sheet->getStyle('A1:G' . $rows)->applyFromArray($styleArray);
        $writer = new Xlsx($spreadsheet);
        $writer->save('files/' . $fileName);
        force_download('files/' . $fileName, NULL);
        redirect('/obat');
    }
    public function upload()
    {
        if (!empty($_FILES['gambar']['name'])) {
            $config['upload_path']          = 'files/upload/';
            $config['allowed_types']        = 'gif|jpg|jpeg|png';
            $config['file_name']            = $_FILES['gambar']['name'];
            $config['overwrite']            = true;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('gambar')) {
                $this->session->set_flashdata('failed', 'File belum di upload');
            } else {
                $data = array('upload_data' => $this->upload->data());
            }
        }
    }
}
