<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url', 'cookie'));
		$this->load->model("Obat_model");
		$this->load->model("Jenis_Obat_model");
		$this->load->model("User_model");
	}
	public function index()
	{
		if (get_cookie('username')) {
			$data = [
				'jenisobat' => $this->Jenis_Obat_model->read(),
				'user' => $this->User_model->readByUsername(get_cookie('username'))[0]->fullname,
				'jumlahjenis' => $this->Jenis_Obat_model->count(),
				'jumlahobat' => $this->Obat_model->count(),
				'jumlahuser' => $this->User_model->count()
			];
			if($this->input->server('REQUEST_METHOD') === 'POST')
			{
				$data['obats'] = $this->Obat_model->search();
			}
			else {
				$data['obats'] = $this->Obat_model->read();
			}

			$this->load->view('dashboard', $data);
		} else {
			redirect('/login');
			return;
		}
	}
}
