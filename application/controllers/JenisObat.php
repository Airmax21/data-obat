<?php
defined('BASEPATH') or exit('No direct script access allowed');

class JenisObat extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Jenis_Obat_model");
        $this->load->helper(array('url','cookie'));
    }
    public function index()
    {
        if (get_cookie('username')) {
        $data['jenisobats'] = $this->Jenis_Obat_model->read();
        $this->load->view('jenisobat/daftar', $data);
        }
        else {
            redirect('/login');
            return;
        }
    }
    public function tambah()
    {
        $jenisobat = $this->Jenis_Obat_model;
        
        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            $jenisobat->create();
            $this->session->set_flashdata('success', 'Berhasil disimpan');
        }
        redirect('/jenisobat');
    }
    public function update($id_jenis_obat)
    {
        $jenisobat = $this->Jenis_Obat_model;
        $data['jenisobat'] = $jenisobat->readById($id_jenis_obat);
        
        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            $jenisobat->update($id_jenis_obat);
            $this->session->set_flashdata('success', 'Berhasil Diubah');
        }
        redirect('/jenisobat');
    }
    public function delete($id_jenis_obat)
    {
        $jenisobat = $this->Jenis_Obat_model;
        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            $jenisobat->delete($id_jenis_obat);
            $this->session->set_flashdata('success', 'Berhasil Dihapus');
        }
        redirect('/jenisobat');
    }
}