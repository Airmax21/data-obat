<?php

use SebastianBergmann\Environment\Console;

ob_start();
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("User_model");
        $this->load->helper(array('url', 'cookie'));
    }
    public function index()
    {
        if (get_cookie('username')) {
            $data['users'] = $this->User_model->read();
            $this->load->view('user/daftar', $data);
        } else {
            redirect('/login');
            return;
        }
    }
    public function tambah()
    {
        $user = $this->User_model;
        $post = $this->input->post();
        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            $username = $user->readByUsername($post['username']);
            if ($username) {
                $this->session->set_flashdata('failed', 'Akun sudah ada');
            } else {
                $user->create();
                $this->session->set_flashdata('success', 'Berhasil disimpan');
            }
        }
        redirect('/user');
    }
    public function register()
    {
        $user = $this->User_model;
        $post = $this->input->post();
        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            $username = $user->readByUsername($post['username']);
            if ($username) {
                $this->session->set_flashdata('failed', 'Akun Sudah Ada');
            }
            elseif($post['password'] !== $post['retype']){
                $this->session->set_flashdata('failed', 'Password Tidak Sama');
            }
            else {
                $user->create();
                $this->session->set_flashdata('success', 'Akun Berhasil di Buat');
            }
        }
        $this->load->view('login/register');
    }
    public function login()
    {

        if ($this->input->cookie('username', TRUE)) {
            redirect('/');
            return;
        }
        $user = $this->User_model;
        $post = $this->input->post();
        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            $users = $user->login();
            if ($users) {
                //     $cookie = array(

                //         'name'   => 'username',
                //         'value'  => $post['username'],
                //         'expire' => 3000,
                //         'secure' => TRUE

                //     );
                set_cookie('username', $post['username'], '36000');
                redirect('/');
                return;
            } else {
                $this->session->set_flashdata('failed', 'Gagal login');
                $this->load->view('login/login');
            }
        } else {
            $this->load->view('login/login');
        }
    }
    public function logout()
    {
        delete_cookie('username');
        redirect('/login');
    }
    public function update($id_user)
    {
        $user = $this->User_model;
        $post = $this->input->post();
        $data['users'] = $user->readById($id_user);

        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            if (isset($post['aktivasi'])) {
                $user->aktivasi($id_user);
                $this->session->set_flashdata('success', 'Berhasil Diaktivasi');
                redirect('/user');
                return;
            }
            $user->update($id_user);
            $this->session->set_flashdata('success', 'Berhasil Diubah');
        }
        redirect('/user');
    }
    public function delete($id_user = null)
    {
        $user = $this->User_model;

        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            $user->delete($id_user);
            $this->session->set_flashdata('success', 'Berhasil Dihapus');
        }
        redirect('/user');
    }
}
