CREATE DATABASE data_obat;
USE data_obat;
CREATE TABLE tb_user (
    id_user int not null AUTO_INCREMENT,
    username VARCHAR(255) not null,
    password VARCHAR(255) not null,
    fullname VARCHAR(255),
    is_active ENUM('0','1'),
    PRIMARY KEY (id_user)
 );
 CREATE TABLE tb_jenis_obat (
     id_jenis_obat int not null AUTO_INCREMENT,
     nama_jenis_obat VARCHAR(255) not null,
     PRIMARY KEY (id_jenis_obat)
);
CREATE TABLE tb_obat (
    id_obat int not null AUTO_INCREMENT,
    id_jenis_obat int not null,
    nama_obat VARCHAR(255) not null,
    satuan int,
    harga int,
    stok int,
    tanggal_expired date,
  	PRIMARY KEY (id_obat),
    FOREIGN KEY (id_jenis_obat) REFERENCES tb_jenis_obat(id_jenis_obat)
);
ALTER TABLE tb_obat ADD gambar BLOB;